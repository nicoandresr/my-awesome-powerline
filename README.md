# Ubuntu
install with `pip install powerline-status`

after of install you must include your bin path to $PATH env var.

  - `export PATH=/home/[user]/.local/bin:$PATH` -> in .bashrc

# Arch Linux
install the packages
```shell
sudo pacman -S powerline powerline-fonts
```

# Install
Just clone this repo in your config path
```shell
git clone git@gitlab.com:nicoandresr/my-awesome-powerline.git powerline
```
